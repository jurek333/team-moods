'use strict';

angular.module('moodsWebApp')
    .directive("average", ["moodConfig", function (moodConfig) {

        function link(scope, element, attrs) {

            function calculateColor(avg) {
                if (avg < 2.5) {
                    return {
                        color: "rgba(210, 10, 10, 1)",
                        lightColor: "rgba(210, 10, 10, 0.2)"
                    };
                } else if (avg > 3.5) {
                    return {
                        color: "rgba(10, 210, 10, 1)",
                        lightColor: "rgba(10, 210, 10, 0.2)"
                    };
                } else {
                    return {
                        color: "rgba(10, 10, 210, 1)",
                        lightColor: "rgba(10, 10, 210, 0.2)"
                    };
                }
            }

            if (undefined === scope.maxValue) {
                var moods = moodConfig.moods;
                scope.maxValue = _.max(moods, 'value').value;
            }

            scope.$watch('avgValues', function(value){

                if (undefined === value) {
                    scope.avgVal = 0;
                    return;
                } else {
                    scope.avgVal = value[0];
                }

                var sum = _.sum(value);
                var data = _.map(value, function (item) {
                    var color = calculateColor(item);
                    return {
                        value: item,
                        color: color.color,
                        highlight: color.lightColor
                    };
                });
                data.push({
                    value: scope.maxValue - sum,
                    color: '#FFFFFF',
                    highlight: '#FFFFFF'
                });

                if (undefined == scope.chart) {
                    var ctx = document.getElementById("averageMoodChart").getContext("2d");
                    scope.chart = new Chart(ctx).Doughnut(data);
                } else {
                    scope.chart.removeData();
                    _.forEach(data, function (item) {
                        scope.chart.addData(item);
                    });
                    scope.chart.update();
                }
            });

            element.on('$destroy', function () {
                scope.chart.destroy();
                scope.chart = undefined;
            });
        }

        return {
            restrict: 'A',
            templateUrl: "stats/average.html",
            scope: {
                avgValues: "=",
                maxValue: "="
            },
            link: link
        };
    }]);
