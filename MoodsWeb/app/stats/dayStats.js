'use strict';

angular.module('moodsWebApp')
	.controller('DayStatsCtrl', ["$scope", "$routeParams", "MoodStats",
	function ($scope, $routeParams, MoodStats) {
	    $scope.day = $routeParams.day;

	    MoodStats.dayAverage($scope.day)
			.success(function (data, status, headers, config) {
			    if (data.length != 0) {
			        $scope.avg = _.map(data, function (item) { return item.average });
			    }
			});
	}]);
