'use strict';

angular.module('moodsWebApp')
	.controller('MoodCtrl', function($scope, $routeParams, $location, Notification, MoodStorage, userService, moodConfig){
		$scope.moods = moodConfig.moods;
    $scope.dayMenuOptions = {
      today: 0,
      yesterday: 1,
      fromCalendar: 2
    };
    $scope.dayMenuOption = $scope.dayMenuOptions.today;
		$scope.newMood = {
			userId: parseInt($routeParams.user),
			value: 3,
			day: moment()//.format(moodConfig.displayDateFormat)
		};
    $scope.anotherDays = [];
    for(var d=2; d<7; ++d) {
      var cd = moment().subtract(d, 'days');
      $scope.anotherDays.push(cd);
    }

		userService.user($scope.newMood.userId)
			.success(function(data){
				$scope.user = data;
			});

		$scope.getCurrentUserName = function() {
      if(undefined === $scope.user)
        return 'Nieznajomy';
			return $scope.user.name;
		};

    $scope.chooseDay = function(menuOption, day) {
      $scope.dayMenuOption = menuOption;
      if(menuOption === $scope.dayMenuOptions.today) {
        $scope.newMood.day = moment();//.format(moodConfig.urlDateFormat)
      } else if (menuOption === $scope.dayMenuOptions.yesterday) {
        $scope.newMood.day = moment().subtract(1,'days');//.format(moodConfig.urlDateFormat)
      } else if (undefined !== day){
        $scope.newMood.day = day;
      }
    };
		$scope.chooseMood = function(newValue){
			$scope.newMood.value = newValue;
		};

		$scope.save = function(){
			var day = $scope.newMood.day;
			MoodStorage.save({
				UserId: $scope.newMood.userId,
				Day: day.format(moodConfig.urlDateFormat),
				MoodValue: $scope.newMood.value
			}).success(function(data, status/*, headers, config*/){
				if(status == 200) {
                    $location.path('/user/'+$routeParams.user+'/average');
				}
			});
		};
	});
