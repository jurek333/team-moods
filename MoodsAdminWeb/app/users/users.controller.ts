module app.users {
  "use strict";

  interface IUsersScope {
    name: string;
    save(): void;
  }

  class UsersController implements IUsersScope {
    name: string;

    constructor() { }

    save(): void { }
  }

  angular
      .module("app.users")
      .controller("app.users.UsersController",UsersController);
}
