var app;
(function (app) {
    var users;
    (function (users) {
        "use strict";
        var UsersController = (function () {
            function UsersController() {
            }
            UsersController.prototype.save = function () { };
            return UsersController;
        })();
        angular
            .module("app.users")
            .controller("app.users.UsersController", UsersController);
    })(users = app.users || (app.users = {}));
})(app || (app = {}));
