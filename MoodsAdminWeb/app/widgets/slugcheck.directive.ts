((): void => {
  "use strict";

  angular
    .module("app.widgets")
    .directive("blSlugCheck", slugCheck);

  slugCheck.$inject = ["app.services.UserService"]
  function slugCheck(userService: app.services.IUserService): ng.IDirective {
    var directive = <ng.IDirective> {
      restrict: 'A',
      link: link
    };

    function link(scope: ng.IScope, element: ng.IAugmentedJQuery): void {
      element.on("blur", (): void => {
          userService.getById("fsdfs").then((user: app.services.IUser): void => {
            console.log(user.name);
            });
        });
    }

    return directive;
  }

  })();
