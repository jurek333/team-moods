module app.widgets {
  "use strict";

    class NewSlugCheckController {
      static $inject = ["$scope"];
      constructor($scope: ng.IScope) {

      }
    }

    angular
      .module("app.blocks")
      .directive("blNewSlugCheck", newSlugCheck)

      newSlugCheck.$inject = ["app.services.UserService"]
      function newSlugCheck(userService: app.services.IUserService): ng.IDirective {
        var directive = <ng.IDirective> {
          restrict: 'A',
          link: link,
          controller: NewSlugCheckController
        };

        function link(scope: ng.IScope, element: ng.IAugmentedJQuery): void {
          element.on("blur", (): void => {
              userService.getById("fsdfs").then((user: app.services.IUser): void => {
                console.log(user.name);
                });
            });
        }

        return directive;
      }


}
