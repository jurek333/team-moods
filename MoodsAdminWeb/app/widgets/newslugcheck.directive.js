var app;
(function (app) {
    var widgets;
    (function (widgets) {
        "use strict";
        var NewSlugCheckController = (function () {
            function NewSlugCheckController($scope) {
            }
            NewSlugCheckController.$inject = ["$scope"];
            return NewSlugCheckController;
        })();
        angular
            .module("app.blocks")
            .directive("blNewSlugCheck", newSlugCheck);
        newSlugCheck.$inject = ["app.services.UserService"];
        function newSlugCheck(userService) {
            var directive = {
                restrict: 'A',
                link: link,
                controller: NewSlugCheckController
            };
            function link(scope, element) {
                element.on("blur", function () {
                    userService.getById("fsdfs").then(function (user) {
                        console.log(user.name);
                    });
                });
            }
            return directive;
        }
    })(widgets = app.widgets || (app.widgets = {}));
})(app || (app = {}));
