interface ICurrentUser {
  userId?: string;
  userName?: string;
}

((): void => {
    "use strict";

    var currentUser:ICurrentUser = {
    };

    angular
      .module("app")
      .value("currentUser", currentUser)

  })();
