﻿using System;

namespace MoodsService
{
	public interface ISystemService
	{
		void Start();
		void Stop();
	}
}

