using System;

namespace MoodsService
{
	public class Sprint 
	{
		public long SprntNo { get; set; }
		public DateTime Start { get; set; }
		public DateTime? End { get; set; }
		public string Name { get; set; }	
	}

	public class SprintMood:Sprint
    {
		public float Average {get; set; }

		public SprintMood(){}
		public SprintMood(Sprint sprint)
		{
			this.Start = sprint.Start;
			this.End = sprint.End;
			this.Name = sprint.Name;
			this.SprntNo = sprint.SprntNo;
		}
	}
}
