using System;

namespace MoodsService
{
	public class MoodMsg 
	{
		public int UserId { get; private set; }
		public DateTime Day { get; private set; }
		public int MoodValue { get; private set; }
		public DateTime Timestamp { get; private set;}

		public MoodMsg(){}
		public MoodMsg(MoodData data){
			UserId = data.UserId;
			Day = data.Day;
			MoodValue = data.MoodValue;
			Timestamp = data.Timestamp;
		}

		public MoodData GetData()
		{
			return new MoodData(){
				Day = this.Day,
				MoodValue = this.MoodValue,
				Timestamp = this.Timestamp,
				UserId = this.UserId
			};
		}
	}
	
}
