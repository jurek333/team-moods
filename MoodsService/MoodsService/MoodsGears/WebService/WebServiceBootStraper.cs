﻿using System;
using Nancy.Bootstrappers.Autofac;
using Autofac;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Responses.Negotiation;

namespace MoodsService
{
	public class WebServiceBootStraper : AutofacNancyBootstrapper
	{
		IActorPublicator _actorPublisher;

		public WebServiceBootStraper(IActorPublicator actorPublisher)
		{
			_actorPublisher = actorPublisher;
		}

		protected override NancyInternalConfiguration InternalConfiguration
		{
			get
			{
				var processors = new[]
				{
					typeof(JsonProcessor),
					typeof(XmlProcessor)
				};

				return NancyInternalConfiguration.WithOverrides(x => x.ResponseProcessors = processors);
			}
		}

		protected override void ApplicationStartup (ILifetimeScope container, Nancy.Bootstrapper.IPipelines pipelines)
		{
			base.ApplicationStartup (container, pipelines);
			StaticConfiguration.DisableErrorTraces = false;
		}

		protected override void ConfigureApplicationContainer (ILifetimeScope existingContainer)
		{
			base.ConfigureApplicationContainer (existingContainer);
			existingContainer.Update (builder => {
				builder.Register(r=>SettingsFactory.CreateFromConfigFile()).As<ServiceSettings> ()
					.InstancePerRequest();
				builder.RegisterInstance(_actorPublisher).As<IActorPublicator>()
					.SingleInstance();
			});
		}
		protected override void ConfigureRequestContainer (ILifetimeScope container, NancyContext context)
		{
			base.ConfigureRequestContainer (container, context);
            container.Update(builder =>
            {
                builder.RegisterType<MySqlMoodTable>().As<IMoodStorage>()
                    .InstancePerRequest();
                builder.RegisterType<MySqlDayAverageMoodTable>().As<IDayAverageMoodStorage>()
                    .InstancePerRequest();
                builder.RegisterType<MySqlUserAverageMoodTable>().As<IUserAverageMoodStorage>()
                    .InstancePerRequest();
                builder.RegisterType<MySqlMoodUsersTable>().As<IMoodUsersTableStorage>()
                    .InstancePerRequest();
                builder.RegisterType<MySqlSprintTable>().As<ISprintTableStorage>()
                    .InstancePerRequest();
            });
		}
	}
}

