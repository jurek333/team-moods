using System;
using Nancy;
using System.Threading.Tasks;
using Nancy.ModelBinding;
using System.Collections.Generic;

namespace MoodsService
{
	public class UserModule : NancyModule
	{
		private IMoodUsersTableStorage _storage;
		public UserModule (IMoodUsersTableStorage storage):base("/user")
		{
			_storage = storage;

			Get ["/{userId}", true] = async (p, ct) => await GetUser (p.userId);
			Get ["/team/{team}", true] = async (p, ct) => await GetUsers(p.team);
			Get ["/", true] = async (p, ct) => await GetUsers(null);
		}

		private async Task<List<MoodUser>> GetUsers (int? team)
		{
			var users = await _storage.Users (team);
			return users;
		}

		private async Task<MoodUser> GetUser(int userId) 
		{
			var user = await _storage.User (userId);
			return user;
		}
	}
}

