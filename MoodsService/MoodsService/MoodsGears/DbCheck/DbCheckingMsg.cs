using System;
using Akka.Actor;
using Nancy.Hosting.Self;
using Nancy.Bootstrapper;
using Akka.DI.Core;

namespace MoodsService
{
	public class DbCheckingMsg 
	{
		public bool DbIsOk { get; private set; }

		public DbCheckingMsg(bool isOk) {
			DbIsOk = isOk;
		}
	}
	
}
