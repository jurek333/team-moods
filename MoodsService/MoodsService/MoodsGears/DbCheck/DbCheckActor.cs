using System;
using Akka.Actor;
using Nancy.Hosting.Self;
using Nancy.Bootstrapper;
using Akka.DI.Core;

namespace MoodsService
{

	public class DbCheckActor : ReceiveActor 
	{
		public static readonly string ActorName = "DbChecker";
		private IDbCheck _dbChecer { get; set; }

		public DbCheckActor(IDbCheck dbChecer) {
			_dbChecer = dbChecer;
		}

		protected override void PreStart ()
		{
			base.PreStart ();

			_dbChecer.CheckTables ();

			Context.Parent.Tell (new DbCheckingMsg (true), Self);

			Self.GracefulStop (TimeSpan.FromSeconds (5));
		}
	}
	
}
