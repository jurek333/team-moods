﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace MoodsService
{

	public interface IDayAverageMoodStorage
	{
		Task<List<DayAverage>> GetAverage(DateTime since, DateTime until);
        Task<int> UpdateAvgMood(DateTime day, int userCount, double moodSum);
	}
}