﻿using System;
using Topshelf;
using Topshelf.Autofac;
using System.Configuration;

namespace MoodsService
{
	public class Program
	{
		public static void Main()
		{
			string serviceName= ConfigurationManager.AppSettings["ServiceName"];
			string serviceDisplayName= ConfigurationManager.AppSettings["ServiceDisplayName"];
			string serviceDescription= ConfigurationManager.AppSettings["ServiceDescription"];

			HostFactory.Run(x => 
				{
					x.UseLinuxIfAvailable();
					x.UseAutofacContainer(SystemServiceContainerFactory.Create());
					x.Service<ISystemService>(s => 
						{
							s.ConstructUsingAutofacContainer();
							s.WhenStarted(tc => tc.Start()); 
							s.WhenStopped(tc => tc.Stop()); 
						});

					x.RunAsLocalSystem(); 
					x.SetDescription(serviceDescription); 
					x.SetDisplayName(serviceDisplayName); 
					x.SetServiceName(serviceName); 
				}); 
		}
	}
}

