﻿using System.Configuration;

namespace MoodsService
{
    public class SettingsFactory 
	{
        private static string AccuireDbConnectionString()
        {
            const string connectionStringTemplate = "server={0};port={1};database={2};user={3};password={4};";
        
            string server = System.Environment.GetEnvironmentVariable("MOODDB_PORT_3306_TCP_ADDR");
            string port = System.Environment.GetEnvironmentVariable("MOODDB_PORT_3306_TCP_PORT");
            string database = System.Environment.GetEnvironmentVariable("MOODDB_ENV_MYSQL_DATABASE");
            string user = System.Environment.GetEnvironmentVariable("MOODDB_ENV_MYSQL_USER");
            string password = System.Environment.GetEnvironmentVariable("MOODDB_ENV_MYSQL_PASSWORD");
            
            if (string.IsNullOrWhiteSpace(server) || string.IsNullOrWhiteSpace(port) ||
                string.IsNullOrWhiteSpace(database) || string.IsNullOrWhiteSpace(user) ||
                string.IsNullOrWhiteSpace(password))
            {
                return ConfigurationManager.ConnectionStrings["moodMySqlDb"].ConnectionString;
            }
            string connectionString = string.Format(connectionStringTemplate, server, port, database, user, password);
            return connectionString;
        }

		public static ServiceSettings CreateFromConfigFile() {
            

			return new ServiceSettings(){
				DbConnectigString = AccuireDbConnectionString(),
				Name = ConfigurationManager.AppSettings["ServiceName"],
				DisplayName = ConfigurationManager.AppSettings["ServiceDisplayName"],
				Description = ConfigurationManager.AppSettings["ServiceDescription"],
				Url = ConfigurationManager.AppSettings["Url"],
				Port = int.Parse(ConfigurationManager.AppSettings["Port"])
			};
		}
	}

	public class ServiceSettings
	{
		public string Name {get;set;}
		public string DisplayName {get;set;}
		public string Description {get;set;}
		public string Url {get;set;}
		public int Port {get;set;}
		public string DbConnectigString { get; set;}
	}
}

