using System;
using System.Data;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using System.Collections.Generic;
using Dapper;
using System.Linq;

namespace MoodsService
{

	public enum DbOperationStatus:int {
		Ok = 0,
		UnknowError = -1,
		NameToLong = -2,
		ObjectDontExists = -3
	}
	
}
